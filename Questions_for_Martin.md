Collected questions to ask Martin

1. What is the significance of the .POR files?
2. Is there a standard for coding missing values that is used across all datasets?
3. Is there a list of participants per time point?
4. Can materials (e.g. a specific questionnaire) be related to variable names?
5. Can be reconstruct the way that constructs were build?
