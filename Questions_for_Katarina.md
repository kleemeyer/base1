Collected questions for Katarina:

1. Dealing with missing values? (Keep original coding or use binary missing-non-missing)
2. Dealing with outliers?
3. Keep aggregate values?
4. Data description in separate (e.g. .json) file?
