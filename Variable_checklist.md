# Requirements from Katarina 
- Name
- Label
- Instruction (e.g. item wording) (?)
- Valid values
- value labels for valid values (Direction of coding)
- missing values
- value labels for missing values
- Measure/ Instruments for collecting data

# Additional checks 
- Source of the variable
- Data type
- Language (English only)
- Duplicate checks (in case of ID variables)

# Extra
- Outliers (Distribution) 
- at which time points available






