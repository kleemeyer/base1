import pandas as pd
import numpy as np
pd.set_option("display.max_columns", None)

from os import listdir
from os.path import isfile, join

import Levenshtein as lev
import re


def merge_to_existing( df, existing_col, new_col):
    
    merge_flag = False
    df_copy = df.copy() # RETURN DF_COPY IF ANY ISSUE
    
    # CHECK IF COLUMNS CAN BE MERGED
    if len( df[ df[existing_col].notna() & df[new_col].notna() ] ) != 0:
        print("XXX")
        print("ERROR: Columns are not mutually exclusive")
        return df_copy, merge_flag
    
    else:    
        df[existing_col].fillna( df[new_col], inplace = True )
        
        # POST CHECKS     
        original_sum = df_copy[existing_col].notnull().sum() + df_copy[new_col].notnull().sum()
            
        if df[existing_col].notnull().sum() != original_sum:
            print("XXX")
            print("ERROR: issue in post checks")
            return df_copy, merge_flag
        
        else:
            df.drop([new_col], axis = 1, inplace = True)
            print('--> Merge Successful, Deleted cols:', new_col)
            merge_flag = True

            return df, merge_flag
        
        
def replace_from_typodict(df, typo_dict, exception = []):
    '''
    Replace columns in Dataframe based on typo dict.
    Exception is a list of keys you do not want to replace.
    '''
    # Maintain list of delete columns
    deleted_cols = []
   
    for k,v in typo_dict.items():

        if k not in exception:

            print("******************************** col_name:", k , "********************************")
            
            if k not in df.columns:
                df[k] = np.nan
                
            if len(v) == 0:
                    print("No typos to replace")

            elif len(v) > 0:
                for val in v:

                    print("typo:", val)
                    df, flag = merge_to_existing(df, k, val)
                    print("--------------------------------------")

                    if flag == True:
                        deleted_cols.append(val)
                    

            print("*********************************************************************************************")
            print()
            print()
            print()
    
    return df