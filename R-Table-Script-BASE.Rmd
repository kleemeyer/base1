
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
doParallel::registerDoParallel(cores = 4)
```


```{r}
# library required
library(tidyverse)
library(haven)

# fred() function 
fred <- function(path) {
  df_1 <- read_sav(path)
  temp_1 <- read_sav(path, user_na = T)
  
  temp <-
    df_1 %>%
    map_if(is.numeric, as.numeric) %>%
    as.tibble() %>%
    select_if(is.numeric) %>%
    pivot_longer(-id, names_to = "variable name") %>%
    group_by(`variable name`) %>%
    summarise_at("value", list(
      min =  ~ min(., na.rm = T),
      max =  ~ max(., na.rm = T)
    )) %>%
    map_if(is.numeric,  ~ ifelse(is.infinite(.), NA, .)) %>%
    as_tibble() %>%
    map_if(is.numeric, as.integer) %>%
    as_tibble() %>%
    left_join(
      temp_1 %>%
        map(~ head(as.vector(.x[is.na(.x)]), 1)) %>%
        as.matrix() %>%
        as.data.frame() %>%
        rownames_to_column() %>%
        mutate(V1 = as.character(V1)) %>%
        rename(variable = rowname, `NA-labelling` = V1) %>%
        mutate(`NA-labelling` = ifelse(
          !str_detect(`NA-labelling`, regex("^\\d")),
          "No MIssing",
          `NA-labelling`
        )),
      by = c("variable name" = "variable")
    )
  temp
}
```


```{r}
# provide the file location to the function
fred("/Users/mac/Documents/Max-Planck-Institut/BASE/EE/Kat1/ee_t8_raw_final.sav")
# provide the file location to the function
fred("/Users/mac/Documents/Max-Planck-Institut/BASE/EE/Kat1/ee928.sav")
# provide the file location to the function
fred("/Users/mac/Documents/Max-Planck-Institut/BASE/EE/Kat1/EE-T1toT7.sav")
```


```{r}
# want to save that thing to a object
object <- fred("/Users/mac/Documents/Max-Planck-Institut/BASE/EE/Kat1/ee_t8_raw_final.sav")

# want to reuse it later
object

# would like to create a csv file. here object.csv will be the file name. you can change it as your wish
write.csv(object,"Table3.csv")
  ```

```{r}
fred(Users/mac/Documents/Max-Planck-Institut/BASE/EE/Kat1/ee_t8_raw_final.csv)
```




































